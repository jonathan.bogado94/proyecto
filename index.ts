import express, { Express, Request, Response } from "express";
import dotenv from 'dotenv';

//configuracion

dotenv.config();

// express app

const app: Express = express();

const port = process.env.PORT || 8000;

// RUTAS

app.get('/', (req: Request, res:Response)=>{
    //send hola
    res.send('data:message: Goodbye, world');
});

// RUTAS

app.get('/hello', (req: Request, res:Response)=>{
    //send hola
    res.send('Hola Amigo');
});

//EJECUTAR APP

app.listen(port,()=>{
    console.log('servidor:  en linea, puerto:', port)
})

